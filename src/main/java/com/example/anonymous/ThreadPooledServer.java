package com.example.anonymous;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPooledServer {
    final static Logger logger = Logger.getLogger(ThreadPooledServer.class);

    private HashMap<String, String> hmap;
    private Map<String, List<Message>> messages;
    private int serverPort;
    private ServerSocket serverSocket = null;
    private boolean isStopped = false;
    private ExecutorService threadPool =
            Executors.newFixedThreadPool(10);

    public ThreadPooledServer(int port) {
        this.serverPort = port;
        hmap = new HashMap<>();
        messages = new HashMap<>();
    }

    public static void main(String[] args) throws IOException {
        ThreadPooledServer server = new ThreadPooledServer(5050);
        server.run();
    }

    public void run() {
        openServerSocket();
        while (!isStopped()) {
            logger.info("in while");
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
                logger.info("in while 1");
            } catch (IOException e) {
                if (isStopped()) {
                    logger.info("Server Stopped.");
                    break;
                }
                throw new RuntimeException(
                        "Error accepting client connection", e);
            }
            logger.info("Size in Pool " + hmap.size());
            this.threadPool.execute(new WorkerRunnable(clientSocket, hmap, messages));

        }

        this.threadPool.shutdown();
        logger.info("Server Stopped.");
    }

    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort, 10);
            logger.info("Socket open");
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port " + this.serverPort, e);
        }
    }
}

