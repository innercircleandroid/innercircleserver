package com.example.anonymous;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JsonParser {
    final static Logger logger = Logger.getLogger(JsonParser.class);

    @SuppressWarnings("unchecked")
    public static Map<String, JSONObject> parseRequestFromAlice(String request) {
        Map<String, JSONObject> receivers = new HashMap<>();

        JSONObject jsonObject;
        jsonObject = new JSONObject(new JSONTokener(request));
        JSONObject requests = jsonObject.getJSONObject("Requests");
        JSONArray nums = requests.getJSONArray("Recepient_ID");
        Iterator<Object> iterator = nums.iterator();
        JSONObject cred = requests.getJSONObject("Cred");

        while (iterator.hasNext()) {
            String next = (String) iterator.next();
            receivers.put(next, cred);
        }

        return receivers;
    }

    public static String[] parseRequestFromBob(String answer) {

        JSONObject jsonObject;
        String sendTo = "";
        jsonObject = new JSONObject(new JSONTokener(answer));
        JSONObject jsonAnswer = (JSONObject) jsonObject.get("Answer_Location");
        sendTo = jsonAnswer.get("Recepient_name").toString();

        return new String[]{sendTo, answer};
    }

    @SuppressWarnings("unchecked")
    public String[] userDetail(String message) {
        String[] details = new String[2];
        JSONObject jsonObj = new JSONObject(new JSONTokener(message));
        JSONObject answer = jsonObj.getJSONObject("Registration");
        //Log.d("Answer in parser", answer.get("Answer").toString());
        details[0] = answer.get("Sender_ID").toString();
        details[1] = answer.get("Reg_ID").toString();

        return details;
    }
}
