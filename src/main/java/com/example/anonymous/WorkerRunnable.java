package com.example.anonymous;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkerRunnable implements Runnable {
    private final static Logger logger = Logger.getLogger(WorkerRunnable.class);

    private Socket clientSocket = null;
    private JsonParser parser = null;
    private HashMap<String, String> users;
    private Map<String, List<Message>> messages;

    public WorkerRunnable(Socket clientSocket, HashMap<String, String> users, Map<String, List<Message>> messages) {
        this.clientSocket = clientSocket;
        this.users = users;
        this.messages = messages;
        this.parser = new JsonParser();
    }

    public void run() {
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter output = new PrintWriter(clientSocket.getOutputStream(), true);
            long time = System.currentTimeMillis();
            java.util.Date date = new java.util.Date();

            while (true) {
                String messageFromUser = input.readLine();
                if (messageFromUser == null) {
                    logger.debug("Empty message.");
                    break;
                }
                else if (messageFromUser.contains("Requests")) {
                    logger.info("Request from user: " + messageFromUser);
                    Map<String, JSONObject> locReq = JsonParser.parseRequestFromAlice(messageFromUser);
                    for (String user : locReq.keySet()) {
                        sendMessage(user, locReq.get(user));
                    }
                    logger.info(new Timestamp(date.getTime()));
                    locReq.clear();

                    break;
                } else if (messageFromUser.contains("Answer")) {
                    String[] answer = JsonParser.parseRequestFromBob(messageFromUser);
                    Message message = new Message(answer[0], answer[1]);
                    messages.put(message.getRecipient(), Collections.singletonList(message)); // Limited to one message per message at the moment

                    JSONObject data = new JSONObject();
                    data.put("Answer_Location", "received");

                    sendMessage(answer[0], data);
                    logger.info(new Timestamp(date.getTime()));
                    break;
                } else if (messageFromUser.contains("Registration")) {
                    logger.info("Registration from user: " + messageFromUser);
                    String[] details = parser.userDetail(messageFromUser);
                    users.put(details[0], details[1]);
                    output.println("Registrations Successfull");
                    logger.info(new Timestamp(date.getTime()));
                    break;
                } else if (messageFromUser.contains("Check")) {
                    logger.info("Check from user: " + messageFromUser);
                    String name = messageFromUser.substring(messageFromUser.indexOf(":") + 2, messageFromUser.length() - 2);
                    logger.info("Name is " + name);
                    logger.info(new Timestamp(date.getTime()));

                    List<Message> messages = this.messages.remove(name);
                    for (Message message : messages) {
                        output.println(message.getContent());
                        logger.info("Found message to send back");
                    }

                }
            }

            input.close();
            logger.info("Request processed: " + time);
        } catch (IOException e) {
            // report exception somewhere.
            e.printStackTrace();
        }
    }

    private void sendMessage(String recpID, JSONObject data) throws IOException {
        logger.info("Message in send " + data);
        new FirebasePushMessage().data(users.get(recpID), data);
    }
}
