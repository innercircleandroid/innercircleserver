package com.example.anonymous;

import okhttp3.*;
import org.json.JSONObject;

import java.io.IOException;

public class FirebasePushMessage {
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FirebasePushMessage.class);

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public void notification(String to, String message) {
        JSONObject obj = initializeJSON(to);

        JSONObject notification = new JSONObject();
        notification.put("title", "From server");
        notification.put("body", message);
        /*
        "notification" : {
            "body" : "great match!",
                    "title" : "Portugal vs. Denmark",
                    "icon" : "myicon"
        }
        */
        obj.put("notification", notification);

        send(obj);
    }

    public void data(String to, JSONObject data) {
        JSONObject obj = initializeJSON(to);
        obj.put("data", data);
        send(obj);
    }

    private JSONObject initializeJSON(String to) {
        JSONObject obj = new JSONObject();
        obj.put("to", to);
        return obj;
    }

    private void send(JSONObject obj) {
        String json = obj.toString();

        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(RequestBody.create(JSON, json))
                .header("Content-Type", "application/json")
                .header("Authorization", "key=" + "AIzaSyA1SbKiuxnwomVBdL8TBcIh0CdfjPdzZ2o")
                .build();
        OkHttpClient client = new OkHttpClient();
        String body = null;
        try (Response response = client.newCall(request).execute()) {
            if (response.code() != 200) {
                logger.error("Error sending FCM: " + response.code());
            }
            body = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.debug("FCM responded with: " + body);
    }
}
