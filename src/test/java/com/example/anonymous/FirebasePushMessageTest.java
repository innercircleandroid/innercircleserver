package com.example.anonymous;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.common.io.ByteStreams;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Map;

public class FirebasePushMessageTest {

    @Test
    public void testFoo() throws IOException {
        FirebasePushMessage firebasePushMessage = new FirebasePushMessage();
        firebasePushMessage.notification("", "fooBar");
    }

    @Test
    public void testFoo2() throws FileNotFoundException {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("innercircle-android-4762f194105c.json");


        ByteArrayOutputStream e = new ByteArrayOutputStream();
        try {
            ByteStreams.copy(stream, e);
            stream.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        JSONObject jsonData = new JSONObject(e.toString());
        GoogleCredential googleCredential = null;
        try {
            googleCredential = GoogleCredential.fromStream(new ByteArrayInputStream(e.toByteArray()), new NetHttpTransport(), GsonFactory.getDefaultInstance());
        } catch (IOException e1) {
            e1.printStackTrace();
        }


        System.out.println(googleCredential.toString());

        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("innercircle-android-4762f194105c.json");
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setServiceAccount(resourceAsStream)
                .setDatabaseUrl("https://innercircle-android.firebaseio.com")
                .build();
        FirebaseApp.initializeApp(options);

        FirebaseApp app = FirebaseApp.getInstance();
        Map<String, Object> databaseAuthVariableOverride = options.getDatabaseAuthVariableOverride();
    }
}